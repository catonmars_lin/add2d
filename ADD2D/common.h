//
//  common.h
//  ADD2D
//
//  Created by User on 2016/10/30.
//  Copyright © 2016年 Stony Brook University. All rights reserved.
//

#ifndef common_h
#define common_h




//#define NANOGUI_GLAD
#if defined(NANOGUI_GLAD)
#if defined(NANOGUI_SHARED) && !defined(GLAD_GLAPI_EXPORT)
#define GLAD_GLAPI_EXPORT
#endif

#include <glad/glad.h>
#else
#if defined(__APPLE__)
#define GLFW_INCLUDE_GLCOREARB
#else
#define GL_GLEXT_PROTOTYPES
#endif
#endif

#include <GLFW/glfw3.h>

#include <nanogui/nanogui.h>
#include <nanogui/window.h>
#include <nanogui/slider.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <string>
#include <stdlib.h>



#endif /* common_h */
