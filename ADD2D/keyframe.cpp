//
//  keyframe.cpp
//  ADD2D
//
//  Created by User on 2016/11/1.
//  Copyright © 2016年 Stony Brook University. All rights reserved.
//

#include "keyframe.hpp"




void KeyframeAnimation::update(){
    if(isPlaying){
        curTime=glfwGetTime();
        if(curTime-startTime>=30.0){
            isPlaying=false;
            return;
        }
        if(keyframes.size()<1){
            isPlaying=false;
            return;
        }
        if(curTime-startTime>=keyframes.back().time){
            isPlaying=false;
            return;
        }
        
        int nfIdx=-1;
        double aniTime=(curTime-startTime);
        for(int i=0;i<keyframes.size();++i){
            if(aniTime<keyframes[i].time){
                nfIdx=i;
                break;
            }
        }
        
        double pfTime=0.0;
        double nfTime=0.0;
        int pfIdx=0;
        if(nfIdx>-1){
            if(nfIdx==0){
                pfTime=0.0;
                nfTime=keyframes[nfIdx].time;
            }
            else{
                pfIdx=nfIdx-1;
                pfTime=keyframes[pfIdx].time;
                nfTime=keyframes[nfIdx].time;
            }
            
        
            double alpha=(aniTime-pfTime)/(nfTime-pfTime);
            for(int i=0;i<m2d->handlesMoved.size();++i){
                m2d->handlesMoved[i]=(keyframes[nfIdx].handles[i]*alpha) + (keyframes[pfIdx].handles[i]*(1.0-alpha));
                m2d->handlesRotation[i]=
                (keyframes[nfIdx].handlesRotation[i]*alpha) + (keyframes[pfIdx].handlesRotation[i]*(1.0-alpha));
              //  std::cout<<m2d->handlesMoved[i]<<std::endl;
            }
        }
        
    }
}

void KeyframeAnimation::updateManifold(){
    if(isPlaying){
        curTime=glfwGetTime();
        if(curTime-startTime>=30.0){
           // isPlaying=false;
            return;
        }
        if(m2d->exManifold.size()<1){
            //isPlaying=false;
            return;
        }
        if(curTime-startTime>=m2d->exManifold.back().time){
           // isPlaying=false;
            return;
        }
        
        int nfIdx=-1;
        double aniTime=(curTime-startTime);
        for(int i=0;i<m2d->exManifold.size();++i){
            if(aniTime<m2d->exManifold[i].time){
                nfIdx=i;
                break;
            }
        }
        
        double pfTime=0.0;
        double nfTime=0.0;
        int pfIdx=0;
        if(nfIdx>-1){
            if(nfIdx==0){
                pfTime=0.0;
                nfTime=m2d->exManifold[nfIdx].time;
            }
            else{
                pfIdx=nfIdx-1;
                pfTime=m2d->exManifold[pfIdx].time;
                nfTime=m2d->exManifold[nfIdx].time;
            }
            
            
            double alpha=(aniTime-pfTime)/(nfTime-pfTime);
            for(int i=0;i<m2d->handlesMoved.size();++i){
                m2d->ex_t[i]=(m2d->exManifold[nfIdx].t[i]*alpha) + (m2d->exManifold[pfIdx].t[i]*(1.0-alpha));
              //  m2d->handlesRotation[i]=
             //   (keyframes[nfIdx].handlesRotation[i]*alpha) + (keyframes[pfIdx].handlesRotation[i]*(1.0-alpha));
                //  std::cout<<m2d->handlesMoved[i]<<std::endl;
            }
        }
        
    }
}


void KeyframeAnimation::play(){
    isPlaying=true;
    startTime=glfwGetTime();
}

void KeyframeAnimation::addKey(Keyframe kf){
    keyframes.push_back(kf);
    sort(keyframes.begin(),keyframes.end());
}

void KeyframeAnimation::save(const char* pathname){
    FILE* fp=fopen(pathname,"w+");
    
    for(int i=0;i<this->keyframes.size();++i){
        Keyframe kf = keyframes[i];
        fprintf(fp,"kf\t%f\n",kf.time);
        for(int j=0;j<kf.handles.size();++j){
            fprintf(fp,"v\t%f\t%f\t%f\n",kf.handles[j].x(),kf.handles[j].y(),kf.handlesRotation[j]);
        }
        fprintf(fp,"end_kf\n");
    }

}

void KeyframeAnimation::load(const char* pathname){
    std::ifstream infile(pathname);
    std::string line;
    
    keyframes.clear();
    
    std::vector<Eigen::Vector3d> h;
    std::vector<double> hr;
    double t=0;
    while(std::getline(infile,line)){
        const char *ln=line.c_str();
        char buf[100];
        sscanf(ln, "%s\t",buf);
        if(strcmp(buf,"kf")==0){
            h.clear();
            hr.clear();
            sscanf(ln,"kf\t%lf",&t);
        }
        else if(strcmp(buf,"v")==0){
            double v1,v2,r;
            sscanf(ln,"v\t%lf\t%lf\t%lf",&v1,&v2,&r);
            h.push_back(Vector3d(v1,v2,0));
            hr.push_back(r);
        }
        else if(strcmp(buf,"end_kf")==0){
            Keyframe kf;
            kf.time=t;
            kf.handles=h;
            kf.handlesRotation=hr;
            keyframes.push_back(kf);
        }
        
        
    }

}