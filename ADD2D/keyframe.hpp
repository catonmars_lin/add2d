//
//  keyframe.hpp
//  ADD2D
//
//  Created by User on 2016/11/1.
//  Copyright © 2016年 Stony Brook University. All rights reserved.
//

#ifndef keyframe_hpp
#define keyframe_hpp

#include "common.h"
#include "util.hpp"
#include "mesh2D.hpp"

class Keyframe{
public:
    std::vector<Vector3d> handles;
    std::vector<double> handlesRotation;
    double time;
    bool operator<(const Keyframe &rhs) const { return time < rhs.time; }
};

class KeyframeAnimation{
public:
    Mesh2D* m2d;
    double curTime;
    double startTime;
    bool isPlaying;
    
    std::vector<Keyframe> keyframes;
    
    KeyframeAnimation(){isPlaying=false;};
    ~KeyframeAnimation(){};
    
    void addKey(Keyframe kf);
    void play();
    void update();
    void updateManifold();
    
    void save(const char* pathname);
    void load(const char* pathname);
    
};


#endif /* keyframe_hpp */
