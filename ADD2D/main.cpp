/*
 src/example3.cpp -- C++ version of an example application that shows
 how to use nanogui in an application with an already created and managed
 glfw context.
 
 NanoGUI was developed by Wenzel Jakob <wenzel.jakob@epfl.ch>.
 The widget drawing code is based on the NanoVG demo application
 by Mikko Mononen.
 
 All rights reserved. Use of this source code is governed by a
 BSD-style license that can be found in the LICENSE.txt file.
 */

// GLFW
//
#include "common.h"
#include "keyframe.hpp"
#include "mesh2D.hpp"
#include "util.hpp"


using namespace nanogui;

enum test_enum {
    Item1 = 0,
    Item2,
    Item3
};

enum app_mode{
    NORMAL,
    ADD_VERTEX,
    ADD_HANDLE,
    ADD_KEY_HANDLE,
    PLAY
};





bool bvar = true;
int ivar = 12345678;
double dvar = 3.1415926;
float fvar = (float)dvar;
std::string strval = "box2";
std::string strTex = "box2.png";
test_enum enumval = Item2;
Color colval(0.5f, 0.5f, 0.7f, 1.f);

GLFWwindow* window;
Screen *screen = nullptr;
Mesh2D* m2d = nullptr;


KeyframeAnimation ani;
app_mode cur_mode = NORMAL;
double cursor_x=0;
double cursor_y=0;
double prev_cx=0;
double prev_cy=0;
Button* addVertex;
Button* addHandle;
Button* setKeyHandle;
Slider *slider;
Color oldColor[10];
bool showTex= false;

int main(int /* argc */, char ** /* argv */) {
    
    glfwInit();
    glfwSetTime(0);
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    glfwWindowHint(GLFW_SAMPLES, 0);
    glfwWindowHint(GLFW_RED_BITS, 8);
    glfwWindowHint(GLFW_GREEN_BITS, 8);
    glfwWindowHint(GLFW_BLUE_BITS, 8);
    glfwWindowHint(GLFW_ALPHA_BITS, 8);
    glfwWindowHint(GLFW_STENCIL_BITS, 8);
    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
    
    // Create a GLFWwindow object
    window = glfwCreateWindow(1280, 720, "Artistic-Directed Dynamics For 2D Deformation", nullptr, nullptr);
    if (window == nullptr) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    
    
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);
    
    
    // An array of 3 vectors which represents 3 vertices
    static const GLfloat g_vertex_buffer_data[] = {
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        0.0f,  1.0f, 0.0f,
    };
    
    // This will identify our vertex buffer
    GLuint vertexbuffer;
    // Generate 1 buffer, put the resulting identifier in vertexbuffer
    glGenBuffers(1, &vertexbuffer);
    // The following commands will talk about our 'vertexbuffer' buffer
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    // Give our vertices to OpenGL.
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
    
    // Create and compile our GLSL program from the shaders
//    GLuint programID = LoadShaders( "SimpleVertexShader.vs", "SimpleFragmentShader.fs" );

  //  glEnable(GL_DEPTH_TEST);
  //  glDepthFunc(GL_LESS);
    
    // Create a nanogui screen and pass the glfw pointer to initialize
    screen = new Screen();
    screen->initialize(window, true);
    
    
    
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);
    glfwSwapInterval(1);
    glfwSwapBuffers(window);
    
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA ,GL_ONE_MINUS_SRC_ALPHA);
    
    // Create nanogui gui
 //   bool enabled = true;
    FormHelper *gui = new FormHelper(screen);
    ref<Window> nanoguiWindow = gui->addWindow(Eigen::Vector2i(0, 0), "ToolBox");
   /* gui->addGroup("Basic types");
    gui->addVariable("bool", bvar)->setTooltip("Test tooltip.");
    gui->addVariable("string", strval);
    
    gui->addGroup("Validating fields");
    gui->addVariable("int", ivar)->setSpinnable(true);
    gui->addVariable("float", fvar)->setTooltip("Test.");
    gui->addVariable("double", dvar)->setSpinnable(true);
    
    gui->addGroup("Complex types");
    gui->addVariable("Enumeration", enumval, enabled)->setItems({ "Item 1", "Item 2", "Item 3" });
    gui->addVariable("Color", colval);
    */
   // gui->addGroup("Other widgets");
    gui->addVariable("Savefile Name", strval);
    gui->addVariable("Texture Name", strTex);
    
    gui->addButton("Load Texture", []() {
        std::cout << "Load Texture." << std::endl;
        if(m2d)
            m2d->loadTexture(strTex);
        showTex=true;
    });
    
    addVertex = gui->addButton("Add Vertex", []() {
        std::cout << "Button pressed." << std::endl;
        cur_mode=ADD_VERTEX;
        oldColor[0] = addVertex->backgroundColor();
        addVertex->setBackgroundColor(Color(Vector4f(0.8,0.2,0.2,1.0)));
        
    });
    addHandle = gui->addButton("Add Handle", []() {
        std::cout << "Add Handle." << std::endl;
        cur_mode=ADD_HANDLE;
        oldColor[1] = addHandle->backgroundColor();
        addHandle->setBackgroundColor(Color(Vector4f(0.8,0.2,0.2,1.0)));
        
    });
    
    setKeyHandle = gui->addButton("Set Keyframe Handle", []() {
        std::cout << "Start set keyFrame Handle." << std::endl;
        cur_mode=ADD_KEY_HANDLE;
        oldColor[1] = setKeyHandle->backgroundColor();
        setKeyHandle->setBackgroundColor(Color(Vector4f(0.8,0.2,0.2,1.0)));
    });

    
    gui->addButton("Add as Example", []() {
        std::cout << "Add Example." << std::endl;
        double curTime=slider->value()*30.0;
        if(m2d)
            m2d->addCurrentPoseToExample(curTime);
    });
    
    gui->addButton("Triangularize", []() {
        std::cout << "Trianglize pressed." << std::endl;
        if(m2d)
            m2d->triangularize();
        showTex=false;
        
    });
    
    gui->addButton("Save", []() {
        std::cout << "Save pressed." << std::endl;
        if(m2d){
            m2d->save(strval.c_str());
            if(ani.keyframes.size()>0){
                ani.save((strval+".ani").c_str());
            }
        }
        
    });
    
    gui->addButton("Load", []() {
        std::cout << "Load pressed." << std::endl;
        
        if(m2d){
            m2d->load(strval);
            ani.load((strval+".ani").c_str());
        }
    });
    
    gui->addButton("Setup", []() {
        std::cout << "Setup pressed." << std::endl;
        if(m2d){
            m2d->assignWeight();
            m2d->setupMatrices();
        }
        
    });


    
   // Slider *slider = new Slider(nanoguiWindow);
   // slider->setValue(0.5f);
   // slider->setFixedWidth(80);
    Window *twin = new Window(screen, "Timeline");
    twin->setPosition(Vector2i(100, 15));
    twin->setLayout(new GroupLayout());

    Widget *panel = new Widget(twin);
    panel->setLayout(new BoxLayout(Orientation::Horizontal,
                                   Alignment::Middle, 0, 10));
    
    slider = new Slider(panel);
    slider->setValue(0.0f);
    slider->setFixedWidth(850);
    
    TextBox *textBox = new TextBox(panel);
    textBox->setFixedSize(Vector2i(60, 25));
    textBox->setValue("0");
    textBox->setUnits("s");
    
    Button *addKeyBtn = new Button(panel);
    addKeyBtn->setCaption(" + ");
    addKeyBtn->setFixedSize(Vector2i(60,25));
    addKeyBtn->setFontSize(20);
    addKeyBtn->setCallback([](){
        Keyframe kf;
        kf.handles = m2d->getHandles();
        kf.handlesRotation =m2d->handlesRotation;
        double time=0.0;
        time=slider->value()*30.0;
        kf.time=time;
        ani.addKey(kf);
        std::cout << "Keyframe Added: " << (double)time << std::endl;
        
    });
    
    Button *playBtn = new Button(panel);
    playBtn->setCaption(" Play ");
    playBtn->setFixedSize(Vector2i(60,25));
    playBtn->setFontSize(20);
    playBtn->setCallback([](){
        m2d->resetPhysics();
        m2d->isPhysics=true;
        ani.play();
        std::cout << "Play start! " << std::endl;
        
    });
    
    Button *playBtn2 = new Button(panel);
    playBtn2->setCaption(" NoPhysics ");
    playBtn2->setFixedSize(Vector2i(60,25));
    playBtn2->setFontSize(20);
    playBtn2->setCallback([](){
   //     m2d->setupMatrices();
        m2d->isPhysics=false;
        m2d->drawWires=false;
        ani.play();
      //  m2d->updateOneFrame();
        std::cout << "Play start! " << std::endl;
        
    });
    
    slider->setCallback([textBox](float value) {
        textBox->setValue(std::to_string((double) (value * 30.0)));
    });
    
    slider->setFinalCallback([&](float value) {
        std::cout << "Final slider value: " << (double) (value * 30.0) << std::endl;
    });
    
    
    
    
    textBox->setFixedSize(Vector2i(60,25));
    textBox->setFontSize(20);
    textBox->setAlignment(TextBox::Alignment::Left);
    twin->center();
    Vector2i pOld=twin->position();
    twin->setPosition(Vector2i(pOld.x(),630));
    
    screen->setVisible(true);
    screen->performLayout();
    nanoguiWindow->center();
    nanoguiWindow->setPosition(Vector2i(0,0));
    nanoguiWindow->setVisible(true);
    
    glfwSetCursorPosCallback(window,
                             [](GLFWwindow *, double x, double y) {
                                 screen->cursorPosCallbackEvent(x, y);
                                 cursor_x=x;
                                 cursor_y=y;
                                 double dx = x-prev_cx;
                                 double dy = y-prev_cy;
                                 Vector2d v=cursorToViewport(Vector2d(cursor_x,cursor_y),screen->width(),screen->height());
                                 if(cur_mode==NORMAL  && (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)==GLFW_PRESS)){
                                     
                                     
                                     int h=m2d->nearestHandle(v);
                                     if(h>-1){
                                         if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL)==GLFW_PRESS)
                                         {
                                             
                                             m2d->handlesRotation[h]+=(dy*0.01);
                                           //  std::cout<<m2d->handlesRotation[h];
                                             m2d->updateHandleTransform(h,dy*0.01);

                                         }
                                         else
                                             m2d->setHandleTransform(h,v);
                                     }
                                     
                                 }
                                 
                             }
                             );
    
    glfwSetMouseButtonCallback(window,
                               [](GLFWwindow *, int button, int action, int modifiers) {
                                   screen->mouseButtonCallbackEvent(button, action, modifiers);
                                   Vector2d v=cursorToViewport(Vector2d(cursor_x,cursor_y),screen->width(),screen->height());
                                   
                                   //    printf("curPos: %f %f\n",v.x(),v.y());
                                   if(cur_mode==ADD_VERTEX && button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS){
                                       m2d->addVertex(Eigen::Vector3d(v.x(),v.y(),0));
                                   }
                                   else if(cur_mode==ADD_HANDLE && button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS){
                                       m2d->addHandle(Eigen::Vector3d(v.x(),v.y(),0));
                                   }
                                   else if(cur_mode==ADD_KEY_HANDLE && button ==GLFW_MOUSE_BUTTON_LEFT && action ==GLFW_PRESS){
                                       int h=m2d->nearestHandle(v);
                                       if(h>-1){
                                           m2d->setKeyframeHandle(h,v);
                                       }

                                   }

                               }
                               );
    
    glfwSetKeyCallback(window,
                       [](GLFWwindow *, int key, int scancode, int action, int mods) {
                           screen->keyCallbackEvent(key, scancode, action, mods);
                           if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE){
                               cur_mode=NORMAL;
                               addVertex->setBackgroundColor(oldColor[0]);
                               addHandle->setBackgroundColor(oldColor[1]);
                               setKeyHandle->setBackgroundColor(oldColor[1]);
                           }
                           if (key == GLFW_KEY_TAB && action == GLFW_RELEASE){
                               if(m2d)
                                   m2d->exManifold.clear();
                           }
                           if (key == GLFW_KEY_LEFT_SHIFT && action == GLFW_RELEASE){
                               if(m2d)
                                   m2d->drawWires=!m2d->drawWires;
                           }
                       }
                       );
    
    glfwSetCharCallback(window,
                        [](GLFWwindow *, unsigned int codepoint) {
                            screen->charCallbackEvent(codepoint);
                        }
                        );
    
    glfwSetDropCallback(window,
                        [](GLFWwindow *, int count, const char **filenames) {
                            screen->dropCallbackEvent(count, filenames);
                        }
                        );
    
    glfwSetScrollCallback(window,
                          [](GLFWwindow *, double x, double y) {
                              screen->scrollCallbackEvent(x, y);
                          }
                          );
    
    glfwSetFramebufferSizeCallback(window,
                                   [](GLFWwindow *, int width, int height) {
                                       screen->resizeCallbackEvent(width, height);
                                   }
                                   );
    
    
   
    
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(
                          0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                          3,                  // size
                          GL_FLOAT,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );
    

    m2d=new Mesh2D();
    ani.m2d=m2d;
    // Game loop
   
    while (!glfwWindowShouldClose(window)) {
        
        double fsTime=glfwGetTime();
        if(ani.isPlaying){
         //   m2d->drawWires=false;
            ani.update();
            ani.updateManifold();
            m2d->update();
            
        }
        else{
         //   m2d->drawWires=true;
        }
   
        glClearColor(0.8f, 0.8f, 0.8f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

      /*
       
       
        // Use our shader
        glUseProgram(programID);
        glBindVertexArray(VertexArrayID);
        
        // 1rst attribute buffer : vertices
        glEnableVertexAttribArray(0);
        
        // Draw the triangle !
        glDrawArrays(GL_TRIANGLES, 0, 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
        glDisableVertexAttribArray(0);
*/
        if(showTex)
            m2d->drawRefTex();
        
        m2d->draw();
        // Draw nanogui
        screen->drawWidgets();
        
        glfwSwapBuffers(window);
        // Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
        glfwPollEvents();
        double curTime=glfwGetTime();
        double fps=1.0/(curTime-fsTime);
        twin->setTitle(std::to_string((int)fps));
    //    std::cout<<"FPS: "<<fps<<std::endl;
        prev_cx = cursor_x;
        prev_cy = cursor_y;
    }
    
    // Terminate GLFW, clearing any resources allocated by GLFW.
    glfwTerminate();
    delete m2d;
    return 0;
}