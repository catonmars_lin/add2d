//
//  mesh2D.cpp
//  ADD2D
//
//  Created by User on 2016/10/23.
//  Copyright © 2016年 Stony Brook University. All rights reserved.
//

#include "mesh2D.hpp"
#include <triangle.h>


std::string swt="pzqDe";
struct triangulateio in, out, mid, vorout;

#define PI 3.14159265


//using namespace mosek::fusion;
//using namespace monty;

static void MSKAPI printstr(void *handle,
                            const char str[])
{
    printf("%s",str);
} /* printstr */

int Mesh2D::mosekOptimize(int wi,int hIdx , std::vector<int> handleIdx, Eigen::SparseMatrix<double> LMLInv){
    
    Eigen::VectorXd Wj;
    Wj.resize(vertexArrayRest.size());
    
    const MSKint32t   numvar = (int)vertexArrayRest.size();
    const MSKint32t   numcon = 0;
    const MSKint32t   numqnz = LMLInv.nonZeros();
    Eigen::VectorXd bigC;
    bigC.resize(vertexArrayRest.size());
    bigC.setZero();
    
    double        *c   = bigC.data();
 /*
    MSKboundkeye  bkc[] = {MSK_BK_LO};
    double        blc[] = {1.0};
    double        buc[] = {+MSK_INFINITY};
    
    MSKboundkeye  bkx[] = {MSK_BK_RA,
        MSK_BK_RA,
        MSK_BK_RA};
    double        blx[] = {0.0,
        0.0,
        0.0};
    double        bux[] = {1.0,
        1.0,
        1.0};
    
    MSKint32t     aptrb[] = {0,   1,   2},
    aptre[] = {1,   2,   3},
    asub[]  = {0,   0,   0};
    double        aval[]  = {1.0, 1.0, 1.0};
    */
    MSKint32t     qsubi[numqnz];
    MSKint32t     qsubj[numqnz];
    double        qval[numqnz];
    
    MSKint32t     j;
    double        xx[numvar];
    
    MSKenv_t      env = NULL;
    MSKtask_t     task = NULL;
    MSKrescodee   r;
    
    /* Create the mosek environment. */
    r = MSK_makeenv(&env,NULL);
    
    if ( r==MSK_RES_OK )
    {
        /* Create the optimization task. */
        r = MSK_maketask(env,numcon,numvar,&task);
        
        if ( r==MSK_RES_OK )
        {
            r = MSK_linkfunctotaskstream(task,MSK_STREAM_LOG,NULL,printstr);
            
            /* Append 'NUMCON' empty constraints.
             The constraints will initially have no bounds. */
            if ( r == MSK_RES_OK )
                r = MSK_appendcons(task,numcon);
            
            /* Append 'NUMVAR' variables.
             The variables will initially be fixed at zero (x=0). */
            if ( r == MSK_RES_OK )
                r = MSK_appendvars(task,numvar);
            
            /* Optionally add a constant term to the objective. */
            if ( r ==MSK_RES_OK )
                r = MSK_putcfix(task,0.0);
            for(j=0; j<numvar && r == MSK_RES_OK; ++j)
            {
                /* Set the linear term c_j in the objective.*/
                if(r == MSK_RES_OK)
                    r = MSK_putcj(task,j,c[j]);
                
                /* Set the bounds on variable j.
                 blx[j] <= x_j <= bux[j] */
                if(r == MSK_RES_OK){
                    MSKboundkeye bk;
                    double bu,bl;
                    if(j==hIdx){
                        bk = MSK_BK_FX;
                        bu = 1.0;
                        bl = 1.0;
                    }
                    else if(std::find(handleIdx.begin(),handleIdx.end(),j) != handleIdx.end()){
                        bk = MSK_BK_FX;
                        bu = 0.0;
                        bl = 0.0;
                    }
                    else{
                        bk = MSK_BK_RA;
                        bu = 1.0;
                        bl = 0.0;
                    }
                    r = MSK_putvarbound(task,
                                        j,           /* Index of variable.*/
                                        bk,      /* Bound key.*/
                                        bl,      /* Numerical value of lower bound.*/
                                        bu);     /* Numerical value of upper bound.*/
                }
                
                /* Input column j of A */
                //    if(r == MSK_RES_OK)
                //        r = MSK_putacol(task,
                //                        j,                 /* Variable (column) index.*/
                //                        aptre[j]-aptrb[j], /* Number of non-zeros in column j.*/
                //                        asub+aptrb[j],     /* Pointer to row indexes of column j.*/
                //                        aval+aptrb[j]);    /* Pointer to Values of column j.*/
                
            }
            
            /* Set the bounds on constraints.
             for i=1, ...,NUMCON : blc[i] <= constraint i <= buc[i] */
            //            for(i=0; i<NUMCON && r==MSK_RES_OK; ++i)
            //                r = MSK_putconbound(task,
            //                                    i,           /* Index of constraint.*/
            //                                    bkc[i],      /* Bound key.*/
            //                                    blc[i],      /* Numerical value of lower bound.*/
            //                                    buc[i]);     /* Numerical value of upper bound.*/
            
            if ( r==MSK_RES_OK )
            {
                /*
                 * The lower triangular part of the Q
                 * matrix in the objective is specified.
                 */
                /*
                qsubi[0] = 0;   qsubj[0] = 0;  qval[0] = 2.0;
                qsubi[1] = 1;   qsubj[1] = 1;  qval[1] = 0.2;
                qsubi[2] = 2;   qsubj[2] = 0;  qval[2] = -1.0;
                qsubi[3] = 2;   qsubj[3] = 2;  qval[3] = 2.0;
                */
                
                int cnt =0;
                for (int k=0;k<LMLInv.outerSize();++k){
                    for (SparseMatrix<double>::InnerIterator it(LMLInv,k); it; ++it){
                        qsubi[cnt] = it.row();
                        qsubj[cnt] = it.col();
                        qval[cnt] = it.value();
                        cnt++;
                    }
                }
                
                /* Input the Q for the objective. */
                
                r = MSK_putqobj(task,numqnz,qsubi,qsubj,qval);
            }
            
            if ( r==MSK_RES_OK )
            {
                MSKrescodee trmcode;
                
                /* Run optimizer */
                r = MSK_optimizetrm(task,&trmcode);
                
                /* Print a summary containing information
                 about the solution for debugging purposes*/
                MSK_solutionsummary (task,MSK_STREAM_MSG);
                
                if ( r==MSK_RES_OK )
                {
                    MSKsolstae solsta;
                    int j;
                    
                    MSK_getsolsta (task,MSK_SOL_ITR,&solsta);
                    
                    switch(solsta)
                    {
                        case MSK_SOL_STA_OPTIMAL:
                        case MSK_SOL_STA_NEAR_OPTIMAL:
                            MSK_getxx(task,
                                      MSK_SOL_ITR,    /* Request the interior solution. */
                                      xx);
                            
                            printf("Optimal primal solution\n");
                            for(j=0; j<numvar; ++j){
                                printf("x[%d]: %e\n",j,xx[j]);
                                weightMatrix(j,wi)=xx[j];
                            }
                            
                            break;
                        case MSK_SOL_STA_DUAL_INFEAS_CER:
                        case MSK_SOL_STA_PRIM_INFEAS_CER:
                        case MSK_SOL_STA_NEAR_DUAL_INFEAS_CER:
                        case MSK_SOL_STA_NEAR_PRIM_INFEAS_CER:
                            printf("Primal or dual infeasibility certificate found.\n");
                            break;
                            
                        case MSK_SOL_STA_UNKNOWN:
                            printf("The status of the solution could not be determined.\n");
                            break;
                        default:
                            printf("Other solution status.");
                            break;
                    }
                }
                else
                {
                    printf("Error while optimizing.\n");
                }
            }
            
            if (r != MSK_RES_OK)
            {
                /* In case of an error print error code and description. */
                char symname[MSK_MAX_STR_LEN];
                char desc[MSK_MAX_STR_LEN];
                
                printf("An error occurred while optimizing.\n");
                MSK_getcodedesc (r,
                                 symname,
                                 desc);
                printf("Error %s - '%s'\n",symname,desc);
            }
        }
        MSK_deletetask(&task);
    }
    MSK_deleteenv(&env);
    
    return (r);

}

int Mesh2D::computeBoundedBiharmonicWeight(){
    
    std::vector<int> handleIdx;
    for(int i=0;i<handles.size();++i){
        for(int j=0;j<vertexArrayRest.size();++j){
            if(handles[i]==vertexArrayRest[j]){
                handleIdx.push_back(j);
                break;
            }
        }
    }
    
    for( auto i:handleIdx)
        std::cout<<i<<std::endl;
    
    for(int i=0;i<vertexNeighbors.size();++i){
        std::cout<<"Vertex :"<<i<<" ";
        for(int j=0;j<vertexNeighbors[i].size();++j){
            std::cout<<vertexNeighbors[i][j]<<" ";
        }
        std::cout<<std::endl;
    }
    
    //Vector2d v1= Vector2d(1.0,0.0);
    //Vector2d v2= Vector2d(0,1.0);
    //std::cout<<"TEST ANGLE: "<<angle2D(v1,v2)<<std::endl;
    Eigen::SparseMatrix<double> L((int)vertexArrayRest.size(),(int)vertexArrayRest.size());
    
    
    int row_L = (int)vertexArrayRest.size();
    int col_L = row_L;
    
    std::vector<int> rows;
    std::vector<int> cols;
    std::vector<double> vals;
    for(int i=0;i<row_L;++i){
        double Cii=0;
        for( int j=0;j< col_L;++j){
            if(i!=j){
                if(contains(vertexNeighbors[i], j)){
                    //j is neighbor of i
                    std::vector<int> N;
                    N = intersec(vertexNeighbors[i],vertexNeighbors[j]);
                    
                    if(N.size()>=1&&N.size()<=2){
                        
                        if(N.size()==1){
                            Vector3d _v1,_v2;
                            _v1 = vertexArrayRest[i]-vertexArrayRest[N[0]];
                            _v2 = vertexArrayRest[j]-vertexArrayRest[N[0]];
                            Vector2d v1,v2;
                            v1=Vector2d(_v1.x(),_v1.y());
                            v2=Vector2d(_v2.x(),_v2.y());
                            
                            double ang =angle2D(v1,v2);
                            if(ang<0)
                                ang=angle2D(v2,v1);
                            double cot = 0.5*(1.0/tan(ang));
                            rows.push_back(i);
                            cols.push_back(j);
                            vals.push_back(cot);
                            L.insert(i,j) = cot;
                            Cii+=cot;
                            
                            
                        }
                        else if(N.size()==2){
                            Vector3d _v1,_v2;
                            _v1 = vertexArrayRest[i]-vertexArrayRest[N[0]];
                            _v2 = vertexArrayRest[j]-vertexArrayRest[N[0]];
                            Vector2d v1,v2;
                            v1=Vector2d(_v1.x(),_v1.y());
                            v2=Vector2d(_v2.x(),_v2.y());
                            double ang =angle2D(v1,v2);
                            if(ang<0)
                                ang=angle2D(v2, v1);
                            double cot = 0.5*(1.0/tan(ang));
                            _v1 = vertexArrayRest[i]-vertexArrayRest[N[1]];
                            _v2 = vertexArrayRest[j]-vertexArrayRest[N[1]];
                            v1=Vector2d(_v1.x(),_v1.y());
                            v2=Vector2d(_v2.x(),_v2.y());
                            ang =angle2D(v1,v2);
                            if(ang<0)
                                ang=angle2D(v2,v1);
                            cot += (1.0/tan(ang));
                            cot *= 0.5;
                            rows.push_back(i);
                            cols.push_back(j);
                            vals.push_back(cot);
                            L.insert(i,j) = cot;
                            Cii+=cot;
                        }
                    }
                    else{
                        std::cout<<"NEIGHBORS NOT FOUND, is your mesh a manifold?\n";
                    }
                    
                }
            }
            
        }
        rows.push_back(i);
        cols.push_back(i);
        vals.push_back(-Cii);
//        L(i,i)=-Cii;
        L.insert(i,i) = -Cii;

    }
    
    Eigen::SimplicialLLT<SparseMatrix<double>> solver;
    solver.compute(L);    //This should be M
    SparseMatrix<double> I((int)vertexArrayRest.size(),(int)vertexArrayRest.size());
    I.setIdentity();
    SparseMatrix<double> L_inv = solver.solve(I);
    
    for(int i=0;i<handles.size();++i){
        int hIdx=-1;
        for(int j=0;j<vertexArrayRest.size();++j){
            if(handles[i]==vertexArrayRest[j]){
                hIdx=j;
                break;
            }
        }
        SparseMatrix<double> sm = L*L;
        SparseMatrix<double> sm2;
        sm2.setZero();
        
        sm2.selfadjointView<Eigen::Lower>() = sm.selfadjointView<Eigen::Lower>();
        std::cout<< sm2.selfadjointView<Lower>();
        mosekOptimize(i,hIdx, handleIdx, sm2); //assume M = I, need to improve
       // break; // only do one optimize
    }
    
/*
    
    auto rp   = monty::new_array_ptr<int,1>(rows.size());
    auto cp   = monty::new_array_ptr<int,1>(cols.size());
    auto vp = monty::new_array_ptr<double,1>(vals.size());
    
    for(int i=0;i<rows.size();++i){
        
        (*rp)[i] =rows[i];
        (*cp)[i] =cols[i];
        (*vp)[i] =vals[i];

      //  std::cout<<i<<std::endl;
    }
    auto ms= mosek::fusion::Matrix::sparse((int)vertexArrayRest.size(),
                                           (int)vertexArrayRest.size(),
                                           rp,
                                           cp,
                                           vp);
    
    std::cout<<ms->toString()<<std::endl;
  
*/
    
    return 0;
}

/*
int testMosek()
{
    auto A1 = monty::new_array_ptr<double,1>({ 3.0, 2.0, 0.0, 1.0 });
    auto A2 = monty::new_array_ptr<double,1>({ 2.0, 3.0, 1.0, 1.0 });
    auto A3 = monty::new_array_ptr<double,1>({ 0.0, 0.0, 3.0, 2.0 });
    auto c  = monty::new_array_ptr<double,1>({ 3.0, 5.0, 1.0, 1.0 });
    mosek::Fusion::Model::t M = new Model("lo1");
    
    auto _M = finally([&]() { M->dispose(); });
    M->setLogHandler([=](const std::string & msg) { std::cout << msg << std::flush; } );
    Variable::t x = M->variable("x", 4, Domain::greaterThan(0.0));
    M->constraint("c1", Expr::dot(A1, x), Domain::equalsTo(30.0));
    M->constraint("c2", Expr::dot(A2, x), Domain::greaterThan(15.0));
    M->constraint("c3", Expr::dot(A3, x), Domain::lessThan(25.0));
    M->objective("obj", ObjectiveSense::Maximize, Expr::dot(c, x));
    M->solve();
    auto sol = x->level();
    std::cout << "[x0,x1,x2,x3] = [" << (*sol)[0] << "," << (*sol)[1] << "," << (*sol)[2] << "]\n";
    return 0;
}
*/

Mesh2D::~Mesh2D(){
    for(int i=0;i<Cs.size();++i){
        delete Cs[i];
    }
}

int Mesh2D::load(std::string pathname) {
    std::ifstream infile(pathname);
    std::string line;
    vertexArray.clear();
    vertexArrayRest.clear();
    vertexNeighbors.clear();
    edgeArray.clear();
    edgeIndexArray.clear();
    uvArray.clear();
    triIndexArray.clear();
    
    handles.clear();
    handlesMoved.clear();
    handleTransforms.clear();
    exManifold.clear();
    IsHandleKeyframed.clear();
    double ex_time=0;
    std::vector<Eigen::Vector3d> r;
    while(std::getline(infile,line)){
        const char *ln=line.c_str();
        char buf[100];
        sscanf(ln, "%s\t",buf);
        if(strcmp(buf,"v")==0){
            double x,y;
            sscanf(ln,"%s\t%lf\t%lf",buf,&x,&y);
            vertexArray.push_back(Vector3d(x,y,0));
            vertexArrayRest.push_back(Vector3d(x,y,0));
        }
        else if(strcmp(buf,"e")==0){
            int x,y;
            sscanf(ln,"%s\t%d\t%d",buf,&x,&y);
            edgeIndexArray.push_back(Vector2i(x,y));
            edgeArray.push_back(vertexArray[x]);
            edgeArray.push_back(vertexArray[y]);
        }
        else if(strcmp(buf,"h")==0){
            double x,y;
            sscanf(ln,"%s\t%lf\t%lf",buf,&x,&y);
            handles.push_back(Vector3d(x,y,0));
            handlesMoved.push_back(Vector3d(x,y,0));
            handlesRotation.push_back(0.0);
            Matrix<double,9,1> m;
            m<<cos(0),sin(0),0,-sin(0),cos(0),0,0,0,1;
            handleTransforms.push_back(m);
            IsHandleKeyframed.push_back(false);
        }
        else if(strcmp(buf,"uv")==0){
            double x,y;
            sscanf(ln,"%s\t%lf\t%lf",buf,&x,&y);
            uvArray.push_back(Vector2d(x,y));
        }
        else if(strcmp(buf,"tex")==0){
            sscanf(ln,"tex\t%s",buf);
            loadTexture(buf);
            haveTex = true;

        }
        else if(strcmp(buf,"tri")==0){
            int v1,v2,v3;
            sscanf(ln,"%s\t%d\t%d\t%d",buf,&v1,&v2,&v3);
            triIndexArray.push_back(Vector3i(v1,v2,v3));
            
        }
        else if(strcmp(buf,"example")==0){
            r.clear();
            sscanf(ln,"example\t%lf",&ex_time);
        }
        else if(strcmp(buf,"ex_h")==0){
            double v1,v2,v3;
            sscanf(ln,"ex_h\t%lf\t%lf\t%lf",&v1,&v2,&v3);
            r.push_back(Vector3d(v1,v2,v3));
        }
        else if(strcmp(buf,"end_ex")==0){
            Example ex(r);
            ex.time=ex_time;
            exManifold.push_back(ex);
        }

        
    }
    return true;
}




int Mesh2D::save(const char* pathname){
   /* edgeIndexArray.clear();
    for(int i=0;i<mid.numberofedges;++i){
        edgeIndexArray.push_back(Vector2i(mid.edgelist[i*2],mid.edgelist[i*2+1]));
        
    }*/

    
    FILE* fp=fopen(pathname,"w+");
    for(int i=0;i<vertexArray.size();++i){
        fprintf(fp,"v\t%f\t%f\n",vertexArrayRest[i].x(),vertexArrayRest[i].y());
    }
    
    for(int i=0;i<handles.size();++i){
        fprintf(fp,"h\t%f\t%f\n",handles[i].x(),handles[i].y());
    }
    
    for (int i=0;i<edgeIndexArray.size();++i){
        fprintf(fp,"e\t%d\t%d\n",edgeIndexArray[i].x(), edgeIndexArray[i].y());
    }

    for (int i=0;i<triIndexArray.size();++i){
        fprintf(fp,"tri\t%d\t%d\t%d\n",triIndexArray[i].x(), triIndexArray[i].y(),triIndexArray[i].z());
    }

    for (int i=0;i<uvArray.size();++i){
        fprintf(fp,"uv\t%f\t%f\n",uvArray[i].x(), uvArray[i].y());
    }
    if(tex&&haveTex)
        fprintf(fp,"tex\t%s\n",tex->textureName().c_str());

    
    for (int i=0;i<exManifold.size();++i){
        fprintf(fp,"example\t%lf\n",exManifold[i].time);
        for(int j=0;j<exManifold[i].r.size();++j){
            Vector3d r=exManifold[i].r[j];
            fprintf(fp,"ex_h\t%f\t%f\t%f\n",r.x(),r.y(),r.z());
        }
        fprintf(fp,"end_ex\n");

    }
    
    
    
    
    fclose(fp);
    return true;
}

int Mesh2D::loadTexture(std::string strTex){
    tex = new GLTexture(strTex);
    auto tex_data = tex->load(strTex);
    
    return true;
}

int Mesh2D::addVertex(Eigen::Vector3d v){
    printf("Vertex %f %f %f\n",v.x(),v.y(),v.z());
    this->vertexArray.push_back(v);
    this->vertexArrayRest.push_back(v);
    
    edgeIndexArray.clear();
    edgeArray.clear();
    if(vertexArray.size()>1){
    for(int i=0;i<vertexArray.size()-1;++i){
        edgeArray.push_back(vertexArray[i]);
        edgeArray.push_back(vertexArray[i+1]);
        edgeIndexArray.push_back(Vector2i(i,i+1));
    }
     //   edgeArray.push_back(vertexArray[vertexArray.size()-1]);
      //  edgeArray.push_back(vertexArray[0]);
    }
    
    return true;
}

int Mesh2D::addHandle(Vector3d h){
    printf("Handle %f %f %f\n",h.x(),h.y(),h.z());
    handles.push_back(h);
    handlesMoved.push_back(h);
    handlesRotation.push_back(0.0);
    Matrix<double,9,1> m;
    m<<cos(0),sin(0),0,-sin(0),cos(0),0,0,0,1;
    handleTransforms.push_back(m);
    IsHandleKeyframed.push_back(false);
    
    
    return true;
}


void Mesh2D::draw(){
    
    glEnable(GL_PROGRAM_POINT_SIZE);
    glBindVertexArray(VertexArrayID);
  //  glEnable(GL_LINE_SMOOTH);
 //   glHint(GL_LINE_SMOOTH_HINT,  GL_NICEST);
    glLineWidth(4.0f);

    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA ,GL_ONE_MINUS_SRC_ALPHA);

   
    // The following commands will talk about our 'vertexbuffer' buffer
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    // Give our vertices to OpenGL.
    glBufferData(GL_ARRAY_BUFFER, vertexArray.size()*sizeof(Vector3d), &vertexArray.front(), GL_STATIC_DRAW);
    
    glVertexAttribPointer(
                          0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                          3,                  // size
                          GL_DOUBLE,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );
    
    // Use our shader
    glUseProgram(programID_point);
    glBindVertexArray(VertexArrayID);
    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    GLint loc=glGetUniformLocation(programID_point, "pointColor");
    glUniform4f(loc,0.8,0.2,0.2,1);
    // Draw the triangle !
    if(drawWires)
    glDrawArrays(GL_POINTS, 0, (GLsizei)vertexArray.size()); // Starting from vertex 0; n vertices
    glDisableVertexAttribArray(0);
   
    
    if(haveTex&&tex){
        // The following commands will talk about our 'vertexbuffer' buffer
        glBindBuffer(GL_ARRAY_BUFFER, uvArrayID);
    // Give our vertices to OpenGL.
        glBufferData(GL_ARRAY_BUFFER, uvArray.size()*sizeof(Vector2d), &uvArray.front(), GL_STATIC_DRAW);
    
    // Use our shader
        glUseProgram(programID_tri);
        glBindVertexArray(VertexArrayID);
    
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex->texture());
    
        glEnableVertexAttribArray(0);
        glBindVertexArray(VertexArrayID);
        // The following commands will talk about our 'vertexbuffer' buffer
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
                          0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                          3,                  // size
                          GL_DOUBLE,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );
    
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, uvArrayID);
        glVertexAttribPointer(
                          1,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                          2,                  // size
                          GL_DOUBLE,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );
    
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triIndexBuffer);
        // Give our vertices to OpenGL.
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, triIndexArray.size()*sizeof(Vector3i), &triIndexArray.front(),
                     GL_STATIC_DRAW);
        
        //  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,triIndexBuffer);
        glDrawElements(GL_TRIANGLES,(int)triIndexArray.size()*3,GL_UNSIGNED_INT,(void*)0);
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
    }
    else{
    // Use our shader
    glUseProgram(programID_tri);
    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    // The following commands will talk about our 'vertexbuffer' buffer
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(
                          0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                          3,                  // size
                          GL_DOUBLE,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triIndexBuffer);
    // Give our vertices to OpenGL.
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, triIndexArray.size()*sizeof(Vector3i), &triIndexArray.front(),
                 GL_STATIC_DRAW);
    
  //  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,triIndexBuffer);
    
        if(drawWires){
    glDrawElements(GL_TRIANGLES,(int)triIndexArray.size()*3,GL_UNSIGNED_INT,(void*)0);
        }
    glDisableVertexAttribArray(0);
    }
    
    
    
    
    
    glBindVertexArray(handleVertexArrayID);
    // The following commands will talk about our 'vertexbuffer' buffer
    glBindBuffer(GL_ARRAY_BUFFER, handleVertexBuffer);
    // Give our vertices to OpenGL.
    glBufferData(GL_ARRAY_BUFFER, handlesMoved.size()*sizeof(Vector3d), &handlesMoved.front(), GL_STATIC_DRAW);
    
    glVertexAttribPointer(
                          0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                          3,                  // size
                          GL_DOUBLE,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );
    
    // Use our shader
    glUseProgram(programID_point);
    glBindVertexArray(handleVertexArrayID);
    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    glUniform4f(loc,0.2,0.8,0.2,1);
    // Draw the triangle !
    if(drawWires)
    glDrawArrays(GL_POINTS, 0, (GLsizei)handlesMoved.size()); // Starting from vertex 0; n vertices
    glDisableVertexAttribArray(0);
    
    
    //Draw Keyframed handles
    std::vector<Vector3d> keyHandles;
    for(int i=0;i<handlesMoved.size();++i){
        if(IsHandleKeyframed[i])
            keyHandles.push_back(handlesMoved[i]);
    }
    
    if(keyHandles.size()>0){
    glBindVertexArray(handleVertexArrayID);
    // The following commands will talk about our 'vertexbuffer' buffer
    glBindBuffer(GL_ARRAY_BUFFER, handleVertexBuffer);
    // Give our vertices to OpenGL.
    glBufferData(GL_ARRAY_BUFFER, keyHandles.size()*sizeof(Vector3d), &keyHandles.front(), GL_STATIC_DRAW);
    
    glVertexAttribPointer(
                          0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                          3,                  // size
                          GL_DOUBLE,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );
    
    // Use our shader
    glUseProgram(programID_point);
    glBindVertexArray(handleVertexArrayID);
    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    glUniform4f(loc,0.2,0.2,1.0,1);
    // Draw the triangle !
    if(drawWires)
    glDrawArrays(GL_POINTS, 0, (GLsizei)keyHandles.size()); // Starting from vertex 0; n vertices
    glDisableVertexAttribArray(0);
    }
    
    
    
    // Use our shader
    glUseProgram(programID_line);
    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    // The following commands will talk about our 'vertexbuffer' buffer
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(
                          0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                          3,                  // size
                          GL_DOUBLE,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, edgeIndexBuffer);
    // Give our vertices to OpenGL.
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, edgeIndexArray.size()*sizeof(Vector2i), &edgeIndexArray.front(), GL_STATIC_DRAW);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,edgeIndexBuffer);
    
    if(drawWires){
        glDrawElements(GL_LINES,(int)edgeIndexArray.size()*2,GL_UNSIGNED_INT,(void*)0);
    }
    glDisableVertexAttribArray(0);

}

void Mesh2D::drawRefTex(){
    
    double w,h;
    int sx,sy;
    w = tex->w;
    h = tex->h;
    glfwGetFramebufferSize(glfwGetCurrentContext(), &sx, &sy);
    double scn_ratio = (double)sx/(double)sy;
    double r = w/h/scn_ratio;
    double s =texScale;
    
    
    const double imgVertex[] = {
        -1*r*s,   1*s,    0,
        -1*r*s,   -1*s,   0,
        1*r*s,    1*s,    0,
        -1*r*s,   -1*s,   0,
        1*r*s,    -1*s,   0,
        1*r*s,    1*s,    0
    };
    
    const double uv[] = {
        0, 0,
        0, 1.0,
        1.0, 0,
        0, 1.0,
        1.0, 1.0,
        1.0, 0
    };
    
    
    
    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer );
    // Give our vertices to OpenGL.
    glBufferData(GL_ARRAY_BUFFER, 6*2*sizeof(double), uv, GL_STATIC_DRAW);
    
    // Use our shader
    glUseProgram(programID_tri);
    glBindVertexArray(texVertexArrayID);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex->texture());
    
    glEnableVertexAttribArray(0);
    glBindVertexArray(texVertexArrayID);
    // The following commands will talk about our 'vertexbuffer' buffer
    glBindBuffer(GL_ARRAY_BUFFER, texVertexbuffer);
    // Give our vertices to OpenGL.
    glBufferData(GL_ARRAY_BUFFER, 6*3*sizeof(double), imgVertex, GL_STATIC_DRAW);
    glVertexAttribPointer(
                          0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                          3,                  // size
                          GL_DOUBLE,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );
    
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
    glVertexAttribPointer(
                          1,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                          2,                  // size
                          GL_DOUBLE,           // type
                          GL_FALSE,           // normalized?
                          0,                  // stride
                          (void*)0            // array buffer offset
                          );

    
    // Draw the triangle !
    glDrawArrays(GL_TRIANGLES, 0, 6); // Starting from vertex 0; n vertices
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    

}

void Mesh2D::triangularize(){
    
    /* Define input points. */
   
    in.numberofpoints = (int) vertexArray.size();
    
    in.numberofsegments = (int)vertexArray.size();
    in.numberofholes = 0;
    in.numberofregions = 0;
    in.segmentlist = (int *) malloc(in.numberofsegments * 2 * sizeof(int));
    for(int i=0;i<in.numberofsegments-1;++i){
        in.segmentlist[2*i]=i;
        in.segmentlist[2*i+1]=i+1;
    }
    in.segmentlist[2*(in.numberofsegments-1)]=(in.numberofpoints-1);
    in.segmentlist[2*(in.numberofsegments-1)+1]=0;

    for(int i=0;i<handles.size();++i){
        vertexArray.push_back(handles[i]);
        vertexArrayRest.push_back(handles[i]);
    }
    in.numberofpoints = (int) vertexArray.size();
    in.numberofpointattributes = 0;
    in.pointlist = (REAL *) malloc(in.numberofpoints * 2 * sizeof(REAL));
    for(int i=0;i<in.numberofpoints;++i){
        in.pointlist[2*i]=vertexArray[i].x();
        in.pointlist[2*i+1]=vertexArray[i].y();
    }
    
    printf("Input point set!\n\n");
  //  report(&in, 1, 0, 0, 0, 0, 0);
    
    /* Make necessary initializations so that Triangle can return a */
    /*   triangulation in `mid' and a voronoi diagram in `vorout'.  */
    
    mid.pointlist = (REAL *) NULL;            /* Not needed if -N switch used. */
    /* Not needed if -N switch used or number of point attributes is zero: */
    mid.pointattributelist = (REAL *) NULL;
    mid.pointmarkerlist = (int *) NULL; /* Not needed if -N or -B switch used. */
    mid.trianglelist = (int *) NULL;          /* Not needed if -E switch used. */
    /* Not needed if -E switch used or number of triangle attributes is zero: */
    mid.triangleattributelist = (REAL *) NULL;
    mid.neighborlist = (int *) NULL;         /* Needed only if -n switch used. */
    /* Needed only if segments are output (-p or -c) and -P not used: */
    mid.segmentlist = (int *) NULL;
    /* Needed only if segments are output (-p or -c) and -P and -B not used: */
    mid.segmentmarkerlist = (int *) NULL;
    mid.edgelist = (int *) NULL;             /* Needed only if -e switch used. */
    mid.edgemarkerlist = (int *) NULL;   /* Needed if -e used and -B not used. */
    
    vorout.pointlist = (REAL *) NULL;        /* Needed only if -v switch used. */
    /* Needed only if -v switch used and number of attributes is not zero: */
    vorout.pointattributelist = (REAL *) NULL;
    vorout.edgelist = (int *) NULL;          /* Needed only if -v switch used. */
    vorout.normlist = (REAL *) NULL;         /* Needed only if -v switch used. */
    
    /* Triangulate the points.  Switches are chosen to read and write a  */
    /*   PSLG (p), preserve the convex hull (c), number everything from  */
    /*   zero (z), assign a regional attribute to each element (A), and  */
    /*   produce an edge list (e), a Voronoi diagram (v), and a triangle */
    /*   neighbor list (n).
     */
    edgeArray.clear();
    edgeIndexArray.clear();
    
    triangulate((char*)swt.c_str(), &in, &mid, &vorout);
    printf("Initial triangulation Done\n\n");
   
    /* Needed only if -r and -a switches used: */
    mid.trianglearealist = (REAL *) malloc(mid.numberoftriangles * sizeof(REAL));
    for( int i=0;i<mid.numberoftriangles;++i)
        mid.trianglearealist[i] = 0.002;

    
    /* Make necessary initializations so that Triangle can return a */
    /*   triangulation in `out'.                                    */
    
    out.pointlist = (REAL *) NULL;            /* Not needed if -N switch used. */
    /* Not needed if -N switch used or number of attributes is zero: */
    out.pointattributelist = (REAL *) NULL;
    out.trianglelist = (int *) NULL;          /* Not needed if -E switch used. */
    /* Not needed if -E switch used or number of triangle attributes is zero: */
    out.triangleattributelist = (REAL *) NULL;
    out.edgelist = (int *) NULL;             /* Needed only if -e switch used. */
    out.edgemarkerlist = (int *) NULL;   /* Needed if -e used and -B not used. */

    /* Refine the triangulation according to the attached */
    /*   triangle area constraints.                       */
    
    triangulate("praezBP", &mid, &out, (struct triangulateio *) NULL);
    
    printf("Refined triangulation:\n\n");
    
    vertexArrayRest.clear();
    vertexArray.clear();
    uvArray.clear();
    for(int i=0;i<out.numberofpoints;++i){
        double x=out.pointlist[2*i];
        double y=out.pointlist[2*i+1];
        vertexArray.push_back(Vector3d(x,y,0));
        vertexArrayRest.push_back(Vector3d(x,y,0));
        
        // Convert world coord To UV
        if(tex){
            double w,h,u,v;
            int sx,sy;
            w = tex->w;
            h = tex->h;
            glfwGetFramebufferSize(glfwGetCurrentContext(), &sx, &sy);
            double scn_ratio = (double)sx/(double)sy;
            double r = w/h/scn_ratio;
            double s = texScale;
            u = 0.5*x/(r*s) +0.5;
            v = 0.5*(1.0-y/(s)) ;
            uvArray.push_back(Vector2d(u,v));
        }
    }
    printf("Num of edges: %d\n",out.numberofedges);
    for(int i=0;i<out.numberofedges;++i){
        Vector3d v1=Vector3d(out.pointlist[2*out.edgelist[i*2]],
                             out.pointlist[2*out.edgelist[i*2]+1],
                             0);
        Vector3d v2=Vector3d(out.pointlist[2*out.edgelist[i*2+1]],
                             out.pointlist[2*out.edgelist[i*2+1]+1],
                             0);
        edgeArray.push_back(v1);
        edgeArray.push_back(v2);
        edgeIndexArray.push_back(Eigen::Vector2i(out.edgelist[i*2],out.edgelist[i*2+1]));

    }
    
    printf("Num of Triangles: %d\n",out.numberoftriangles);
    for(int i=0;i<out.numberoftriangles;++i){
        int i1 = out.trianglelist[3*i];
        int i2 = out.trianglelist[3*i+1];
        int i3 = out.trianglelist[3*i+2];

        triIndexArray.push_back(Vector3i(i1,i2,i3));
        
    }
    
    
    
    
    free(in.pointlist);
    free(in.segmentlist);
    free(mid.pointlist);
    free(mid.pointattributelist);
    free(mid.pointmarkerlist);
    free(mid.trianglelist);
    free(mid.triangleattributelist);
    free(mid.trianglearealist);
    //    free(mid.neighborlist);
    free(mid.segmentlist);
    free(mid.segmentmarkerlist);
    free(mid.edgelist);
    free(mid.edgemarkerlist);
    
    free(out.pointlist);
    free(out.segmentlist);
    free(out.pointattributelist);
    free(out.trianglelist);
    free(out.triangleattributelist);
    free(out.edgelist);
    free(out.edgemarkerlist);
    
    haveTex=true;

}




void Mesh2D::setupMatrices(){
    
    t.resize(9*handles.size());
    for(int i=0;i<handles.size();++i){
        handlesRotation[i] = 0;
        handleTransforms[i](0)=cos(0);
        handleTransforms[i](1)=sin(0);
        handleTransforms[i](2)=0;
        handleTransforms[i](3)=-sin(0);
        handleTransforms[i](4)=cos(0);
        handleTransforms[i](5)=0;
        handleTransforms[i](6)=0;
        handleTransforms[i](7)=0;
        handleTransforms[i](8)=1;

        t(9*i+0) = handleTransforms[i](0);
        t(9*i+1) = handleTransforms[i](1);
        t(9*i+2) = handleTransforms[i](2);

        t(9*i+3) = handleTransforms[i](3);
        t(9*i+4) = handleTransforms[i](4);
        t(9*i+5) = handleTransforms[i](5);

        t(9*i+6) = handleTransforms[i](6);
        t(9*i+7) = handleTransforms[i](7);
        t(9*i+8) = 1; //homogeneous coord
    }
    std::cout<<t<<std::endl;
   
    U2.resize(3*(int)vertexArray.size(),9*(int)handles.size());
    U2.setZero();
    U.resize(3*(int)vertexArray.size(),9*(int)handles.size());
    for(int i=0;i<vertexArray.size();++i){
        for(int j=0;j<handles.size();++j){
            double w=weightMatrix(i,j);
            
            U.insert(3*i   ,9*j )   = vertexArrayRest[i].x() *w;
            U.insert(3*i+1 ,9*j+1 ) = vertexArrayRest[i].x() *w;
            U.insert(3*i+2 ,9*j+2 ) = vertexArrayRest[i].x() *w;

            U.insert(3*i   ,9*j+3 ) = vertexArrayRest[i].y() *w;
            U.insert(3*i+1 ,9*j+4 ) = vertexArrayRest[i].y() *w;
            U.insert(3*i+2 ,9*j+5 ) = vertexArrayRest[i].y() *w;

            U.insert(3*i   ,9*j+6)  = 1.0 *w;
            U.insert(3*i+1 ,9*j+7)  = 1.0 *w;
            U.insert(3*i+2 ,9*j+8)  = 1.0 *w;
        }
    }
    std::cout<<U<<std::endl;
    
    VectorXd x = U*t;
    vertexArray.clear();
    for(int i=0;i<(x.rows()/3);++i){
        printf("V: %f %f\n",x(3*i),x(3*i+1));
        vertexArray.push_back(Vector3d(x(3*i),x(3*i+1),0));
    }

    //Setting up Simulation parameters
    
    tn.resize(2*handles.size());
    for(int i=0;i<handles.size();++i){
        tn(2*i)   = handles[i].x();
        tn(2*i+1) = handles[i].y();
      //  tn(3*i+1) = 0;
    }
    M.resize(2*(int)vertexArray.size(),2*(int)vertexArray.size());
    M.setIdentity();
    
    Mp.resize(2*(int)handles.size(),2*(int)handles.size());
  //  Mp = U.transpose()*M*U;
    Mp.setIdentity();
    Mp*=50.0;
    
    Mp_inv.resize(2*(int)handles.size(),2*(int)handles.size());
    Mp_inv = Mp.inverse();
    
    f_ext.resize(2*(int)vertexArray.size());
    for(int i=0;i<vertexArray.size();++i){
        f_ext(2*i) = 0;
        f_ext(2*i+1) = 0.0; //gravity value
    //    f_ext(3*i+2) = 0;  //For homogeneous coordinate
    }
    f_ext_p.resize(2*handles.size());
  //  f_ext_p = U.transpose()*f_ext;
    for(int i=0;i<handles.size();++i){
        f_ext_p(2*i) = 0;
        f_ext_p(2*i+1) = -0.0; //gravity value
//        f_ext_p(2*i+2) = 0;  //For homogeneous coordinate
    }
    
    Cs.clear();
    for(int i=0;i<handles.size();++i){
        for(int j=i+1;j<handles.size();++j){
            Vector2d p1 = Vector2d(handles[i].x(),handles[i].y());
            Vector2d p2 = Vector2d(handles[j].x(),handles[j].y());
            Constraint* c;
            c = new Constraint(p1,p2,i,j,SPRING,5000.0);
            
            std::vector<Triplet<int>> tris;
            tris.push_back(Triplet<int>(0, i*2   ,1));
            tris.push_back(Triplet<int>(1, i*2+1 ,1));
            tris.push_back(Triplet<int>(2, j*2   ,1));
            tris.push_back(Triplet<int>(3, j*2+1 ,1));
            c->S.resize(2*2,2*(int)handles.size());
            c->S.setFromTriplets(tris.begin(),tris.end());
            
            Cs.push_back(c);
            
        }
    }
    
    ex_t.clear();
    ex_t.reserve(handles.size());
    if(exManifold.size()>0){
        exManifold[0].convToDiffCoord();
        exManifold[1].convToDiffCoord();
        for(int i=0;i<handles.size();++i){
            Vector2d p1 = Vector2d(handlesMoved[i].x(),handlesMoved[i].y());
            Vector2d p2 = Vector2d(exManifold[0].t[i].x(),exManifold[0].t[i].y());
            Vector2d p3 = Vector2d(exManifold[1].t[i].x(),exManifold[1].t[i].y());
            Constraint* c;
            c = new Constraint(p1,p2,p3,i,12000.0);
            
            std::vector<Triplet<int>> tris;
            tris.push_back(Triplet<int>(0, i*2   ,1));
            tris.push_back(Triplet<int>(1, i*2+1 ,1));
            c->S.resize(2*1,2*(int)handles.size());
            c->S.setFromTriplets(tris.begin(),tris.end());
            
            Cs.push_back(c);
        
        }
    }
    
    CKeys.clear();
    for(int i=0;i<handles.size();++i){
        Vector2d p1 = Vector2d(handlesMoved[i].x(),handlesMoved[i].y());
        Constraint* c = new Constraint(p1,p1,i,i,KEYFRAME_HANDLE,4000.0);
        std::vector<Triplet<int>> tris;
        tris.push_back(Triplet<int>(0, i*2   ,1));
        tris.push_back(Triplet<int>(1, i*2+1 ,1));
        c->S.resize(2*1,2*(int)handles.size());
        c->S.setFromTriplets(tris.begin(),tris.end());
        
        CKeys.push_back(c);
    }

    vn.resize(2*handles.size());
    vn.setZero();
    
    sn.resize(2*handles.size());
    tn1.resize(2*handles.size());
    rhs.resize(2*handles.size());
    p.resize(2*handles.size());
  //  lhs.resize(2*handles.size());
    for (int i=0;i<handles.size();++i){
        p(2*i) = 0;
        p(2*i+1) = 0;
  //      p(3*i+2) = 0; //homogeneous coord
    }
 //   Eigen::SparseMatrix<double> S;
 //   S.resize(2*(int)handles.size(),2*(int)handles.size());
//    S.setIdentity();
    Eigen::SparseMatrix<double> Mtmp;
    Mtmp.resize(2*(int)handles.size(),2*(int)handles.size());
    Mtmp = Mp.sparseView();
    Mtmp = Mtmp/(h*h);
    for(int i=0;i<Cs.size();++i){
        Mtmp += (Cs[i]->w * Cs[i]->S.transpose()*Cs[i]->S);
    }
    
    for(int i=0;i<CKeys.size();++i){
        Mtmp += (CKeys[i]->w * CKeys[i]->S.transpose()*CKeys[i]->S);
        
    }
    
    lhs.compute(Mtmp);
    
}

void Mesh2D::simulate(){
    static double mod=1.0;
    mod*=0.995;
    sn = tn + (h*vn) + (h*h*Mp_inv*f_ext_p);
    tn1 = sn;
   /* CKeys.clear();
    for(int i=0;i<handles.size();++i){
        Vector2d p1 = Vector2d(handlesMoved[i].x()-handles[i].x(),handlesMoved[i].y()-handles[i].y());
        Constraint* c = new Constraint(p1,p1,i,i,KEYFRAME_HANDLE,0.5);
        CKeys.push_back(c);
    }*/
    Vector3d com;
   
    std::cout<<tn1<<std::endl;
    
    com.setZero();
    for(int m=0;m<handles.size();++m){
        com+=Vector3d(handlesMoved[m].x(),handlesMoved[m].y(),0);
    }
    com/=(double)handles.size();
    
    double energy1,energy2;
    energy1=0;
    energy2=0;
    if(exManifold.size()>0)
    for(int i=0;i<handlesMoved.size();++i){
        energy1+=((handlesMoved[i]-com)-exManifold[0].t[i]).norm();
        energy2+=((handlesMoved[i]-com)-exManifold[1].t[i]).norm();

    }
    energy1/=mod;
    energy2*=mod;
    
    std::cout<<"E1:"<<energy1<<" E2: "<<energy2<<"\n";
    
    for(int i=0;i<10;++i){
        rhs = (Mp*sn)/(h*h);
        p.setZero();
        
        
        
        for(int j=0;j<Cs.size();++j){
            p.setZero();
            if(Cs[j]->type==SPRING){
                int i1,i2;
                
                i1=Cs[j]->idx_p1;
                i2=Cs[j]->idx_p2;
                Vector2d p1 = Vector2d(tn1(2*i1+0),tn1(i1*2+1));
                Vector2d p2 = Vector2d(tn1(2*i2+0),tn1(i2*2+1));
                Vector2d d = p2-p1;
                double dist = d.norm();
                d.normalize();
                
                if(IsHandleKeyframed[i1]||IsHandleKeyframed[i2]){
                    if(IsHandleKeyframed[i1]){
                        p1=Vector2d(handlesMoved[i1].x(),handlesMoved[i2].y());
                        d=p2-p1;
                        p2=p2 -d*(dist-Cs[j]->rest_length);
                    }
                    if(IsHandleKeyframed[i2]){
                        p2=Vector2d(handlesMoved[i2].x(),handlesMoved[i2].y());
                        d=p2-p1;
                        p1=p1 +d*(dist-Cs[j]->rest_length);
                    }
                }
                else{
                
                    p1 = p1 +d*(dist-Cs[j]->rest_length)/2.0;
                    p2 = p2 -d*(dist-Cs[j]->rest_length)/2.0;
                }
                p(2*i1+0)=(p1.x());
                p(2*i1+1)=(p1.y());  //make it position, can't use transform...
              //  p(3*i1+2)= 0;
                p(2*i2+0)=(p2.x());
                p(2*i2+1)=(p2.y());
              //  p(3*i2+2)= 0;
                rhs += Cs[j]->w*p;
            }
            else if(Cs[j]->type==ATTACH){
                int i1,i2;
                
                i1=Cs[j]->idx_p1;
                i2=Cs[j]->idx_p2;
                Vector2d p1 = Vector2d(tn1(2*i1+0),tn1(i1*2+1));
                Vector2d p2 = Vector2d(tn1(2*i2+0),tn1(i2*2+1));
                Vector2d d = p2-p1;
                double dist = d.norm();
                d.normalize();
                
                p1 = p1 +d*(dist-Cs[j]->rest_length);
              //  p2 = p2 -d*(dist-Cs[j]->rest_length)/2.0;
                p(2*i1+0)=(p1.x());
                p(2*i1+1)=(p1.y());  //make it position, can't use transform...
                //  p(3*i1+2)= 0;
             //   p(2*i2+0)=(p2.x());
             //   p(2*i2+1)=(p2.y());
                //  p(3*i2+2)= 0;
                rhs += Cs[j]->w*p;
            }
            else if(Cs[j]->type==EXAMPLE){
                int i1;
                
                i1=Cs[j]->idx_p1;
                Vector2d center = Vector2d(com.x(),com.y());
                Vector2d p1 = Vector2d(handlesMoved[i1].x(),handlesMoved[i1].y())-center;
                Vector2d p2 = Cs[j]->p2;
                Vector2d p3 = Cs[j]->p3;
                
                                Vector2d d = p3-p2;
               // Vector2d n = Vector2d(d.y(),-d.x());
                
                double m = (p3.y()-p2.y())/(p3.x()-p2.x());
                double b = p2.y()-(m*p2.x());
                double x = (m*  p1.y() + p1.x()-m*b)/(m*m+1);
                double y = (m*m*p1.y() + m*p1.x()+b)/(m*m+1);
                Vector2d proj_p=Vector2d(x,y);
          //      double alpha1=(proj_p-p2).norm()/d.norm();
          //      double alpha2=(proj_p-p3).norm()/d.norm();
                
                if(energy1<energy2){
                    p1 = p2+center;
                  //  std::cout<<"E1<E2\n";
                }
                else{
                    p1 = p3+center;
                  //  std::cout<<"E1>E2\n";

                }
                p1=Vector2d(ex_t[i1].x(),ex_t[i1].y())+center;
             //       p1 = proj_p+com;
            //    p1 = p2+com;
                //  p2 = p2 -d*(dist-Cs[j]->rest_length)/2.0;
                p(2*i1+0)=(p1.x());
                p(2*i1+1)=(p1.y());  //make it position, can't use transform...
                //  p(3*i1+2)= 0;
                //   p(2*i2+0)=(p2.x());
                //   p(2*i2+1)=(p2.y());
                //  p(3*i2+2)= 0;
                rhs += Cs[j]->w*p;
            }


            
        //    std::cout<<"p: "<<p<<std::endl;
        }
        
        
        
        for(int j=0;j<CKeys.size();++j){
            p.setZero();
            if(CKeys[j]->type==KEYFRAME_HANDLE){
                int i1;
                
                i1=CKeys[j]->idx_p1;
                Vector2d p1 = Vector2d(handlesMoved[i1].x(),handlesMoved[i1].y());
                //p1=CKeys[j]->p1;
                
                p(2*i1+0)=(p1.x());
                p(2*i1+1)=(p1.y());  //make it position
                rhs += CKeys[j]->w*p;

            }
            
          //  std::cout<<"p: "<<p<<std::endl;

        }
        
        
        tn1 = lhs.solve(rhs);
        for(int i=0;i<handlesMoved.size();++i){
            if(IsHandleKeyframed[i]){
                tn1(2*i)=handlesMoved[i].x();
                tn1(2*i+1)=handlesMoved[i].y();
            }
        }

    }
    vn = (tn1-tn)/h*0.99;
 //   std::cout<<"vn:"<<vn<<std::endl;
    
}

void Mesh2D::update(){
    
   // isPhysics=false;
    if(isPhysics){
        
        simulate();
    //    std::cout<<"tn  : "<<tn<<std::endl;
    //    std::cout<<"tn1: "<<tn1<<std::endl;
        tn=tn1;
        for(int i=0;i<handles.size();++i){
          //  double r=handlesRotation[i];
            t(9*i)   = 1;
            t(9*i+1) = 0;
            t(9*i+2) = 0;
            t(9*i+3) = 0;
            t(9*i+4) = 1;
            t(9*i+5) = 0;
            t(9*i+6) = tn1(2*i+0)-handles[i].x();
            t(9*i+7) = tn1(2*i+1)-handles[i].y();
            t(9*i+8) = 1; //homogeneous coord
        }

    }
    else{
        for(int i=0;i<handles.size();++i){
            
            Vector3d v=handles[i];
            Vector2d old_v=Vector2d(v.x(),v.y());
            Vector2d nv=Vector2d(handlesMoved[i].x(),handlesMoved[i].y());
            Vector2d trans = nv-old_v;
            
            handleTransforms[i](6) = trans.x();
            handleTransforms[i](7) = trans.y();
            
            
         //   double r=handlesRotation[i];
            t(9*i)   = 1;
            t(9*i+1) = 0;
            t(9*i+2) = 0;
            t(9*i+3) = 0;
            t(9*i+4) = 1;
            t(9*i+5) = 0;
            t(9*i+6) = handleTransforms[i](6);
            t(9*i+7) = handleTransforms[i](7);
            t(9*i+8) = 1; //homogeneous coord
        }
    }
    
    VectorXd x = U*t;
    
    for(int i=0;i<vertexArray.size();++i){
        for(int j=0;j<handles.size();++j){
            double w=weightMatrix(i,j);
            /*
             U2(3*i   ,9*j )   = (vertexArrayRest[i].x()-handlesMoved[j].x()) *w;
             U2(3*i+1 ,9*j+1 ) = (vertexArrayRest[i].x()-handlesMoved[j].x()) *w;
             U2(3*i+2 ,9*j+2 ) = (vertexArrayRest[i].x()-handlesMoved[j].x()) *w;
             
             U2(3*i   ,9*j+3 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()) *w;
             U2(3*i+1 ,9*j+4 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()) *w;
             U2(3*i+2 ,9*j+5 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()) *w;
             */
            U2(3*i   ,9*j )   = (x(3*i) - handlesMoved[j].x()) *w;
            U2(3*i+1 ,9*j+1 ) = (x(3*i) - handlesMoved[j].x()) *w;
            U2(3*i+2 ,9*j+2 ) = (x(3*i) - handlesMoved[j].x()) *w;
            
            U2(3*i   ,9*j+3 ) = (x(3*i+1) - handlesMoved[j].y()) *w;
            U2(3*i+1 ,9*j+4 ) = (x(3*i+1) - handlesMoved[j].y()) *w;
            U2(3*i+2 ,9*j+5 ) = (x(3*i+1) - handlesMoved[j].y()) *w;
            
            U2(3*i   ,9*j+6)  = 1.0 *w;
            U2(3*i+1 ,9*j+7)  = 1.0 *w;
            U2(3*i+2 ,9*j+8)  = 1.0 *w;
        }
    }
    
    
    
    
    
    for(int i=0;i<handles.size();++i){
        double r = handlesRotation[i];
        //     r=0;
        
        t(9*i)   = cos(r);
        t(9*i+1) = sin(r);
        t(9*i+2) = 0;
        t(9*i+3) = -sin(r);
        t(9*i+4) = cos(r);
        t(9*i+5) = 0;
        //     t(9*i+6) = handlesMoved[i].x()+handleTransforms[i](6);
        //     t(9*i+7) = handlesMoved[i].y()+handleTransforms[i](7);
        t(9*i+6) = handlesMoved[i].x();
        t(9*i+7) = handlesMoved[i].y();
        t(9*i+8) = 1; //homogeneous coord
        
        /*    if(i==h){
         t(9*i+6) = handlesMoved[i].x()+handleTransforms[i](6);
         t(9*i+7) = handlesMoved[i].y()+handleTransforms[i](7);
         }*/
    }
    
    
    x = U2*t;

    
    
    
    
    
    
    
    vertexArray.clear();
    for(int i=0;i<(x.rows()/3);++i){
   //     printf("V: %f %f\n",x(3*i),x(3*i+1));
        vertexArray.push_back(Vector3d(x(3*i),x(3*i+1),0));
    }

}

void Mesh2D:: updateOneFrame(){
    update();
}


void Mesh2D::assignWeight(){
    vertexNeighbors.clear();
    for(int i=0;i<vertexArrayRest.size();++i){
        std::vector<int> neighbors;
        vertexNeighbors.push_back(neighbors);
    }
    for(int i=0;i<edgeIndexArray.size();++i){
        Vector2i eIdx=edgeIndexArray[i];
        vertexNeighbors[eIdx.x()].push_back(eIdx.y());
        vertexNeighbors[eIdx.y()].push_back(eIdx.x());
    }
    weightMatrix.resize(vertexArray.size(),handles.size());
    computeBoundedBiharmonicWeight();
    for(int i=0;i<vertexArray.size();++i){
        double sumRow=0.0;
        for(int j=0;j<handles.size();++j){
            double w = weightMatrix(i,j);
            sumRow+=w;
        }
        
        for(int j=0;j<handles.size();++j){
            weightMatrix(i,j)/=sumRow;
        }
    }
    
  /*
    for(int i=0;i<vertexArray.size();++i){
        double sumRow=0.0;
        for(int j=0;j<handles.size();++j){
            
             double w=1.0/(double)handles.size();
             
             double dist = (vertexArray[i]-handlesMoved[j]).norm();
             double handleRadius = 0.5;
             w = 1.0-((dist*dist)/(handleRadius*handleRadius));
             if (w<=0.0)
                 w=0.0;
            
            weightMatrix(i,j)=w;
            sumRow+=w;
        }
        
        for(int j=0;j<handles.size();++j){
            weightMatrix(i,j)/=sumRow;
        }
    }*/
    
}

int Mesh2D::nearestHandle(Vector2d v){
  //  double min_dist=99999.0;
    
    double point_radius=2.0*20.0/1280.0;
    for(int i=0;i<handlesMoved.size();++i){
        Vector2d h=Vector2d(handlesMoved[i].x(),handlesMoved[i].y());
        Vector2d res=(h-v);
        double dist=res.norm();
        if(dist<point_radius){
            return i;
        }
    }
    return -1;
}

void Mesh2D::resetPhysics(){
    tn.setZero();
    for(int i=0;i<handles.size();++i){
        tn(2*i)   = handles[i].x();
        tn(2*i+1) = handles[i].y();
        //  tn(3*i+1) = 0;
    }
    vn.setZero();
}

void Mesh2D::addCurrentPoseToExample(double time){
    Example ex(handlesMoved);
    ex.time=time;
    exManifold.push_back(ex);
}

void Mesh2D::setKeyframeHandle(int i,Eigen::Vector2d v){
    IsHandleKeyframed[i]=!IsHandleKeyframed[i];
    
}

void Mesh2D::setHandleTransform(int h,Eigen::Vector2d nv){
  
    Vector3d v=handles[h];
    Vector2d old_v=Vector2d(v.x(),v.y());
    Vector2d trans = nv-old_v;
    //   int pos = (int)(find(handlesMoved.begin(), handlesMoved.end(), *v) - handlesMoved.begin());
    handleTransforms[h](6) = trans.x();
    handleTransforms[h](7) = trans.y();
    handlesMoved[h]=Vector3d(nv.x(),nv.y(),0);

    for(int i=0;i<handles.size();++i){
        handleTransforms[i](6) = handlesMoved[i].x()-handles[i].x();
        handleTransforms[i](7) = handlesMoved[i].y()-handles[i].y();
        //  double r = handlesRotation[i];
        //  double r;
        //  if(h==i)
        //      r=delta_rot;
        //  else
        //      r=0;
        t(9*i)   = 1;
        t(9*i+1) = 0;
        t(9*i+2) = 0;
        t(9*i+3) = 0;
        t(9*i+4) = 1;
        t(9*i+5) = 0;
        t(9*i+6) = handleTransforms[i](6);
        t(9*i+7) = handleTransforms[i](7);
        //      t(9*i+6) = handlesMoved[i].x();
        //      t(9*i+7) = handlesMoved[i].y();
        //       t(9*i+6) = handleTransforms[i](6)-handlesMoved[i].x();
        //       t(9*i+7) = handleTransforms[i](7)-handlesMoved[i].y();
        t(9*i+8) = 1; //homogeneous coord
    }
    
    VectorXd x = U*t;
    
    vertexArrayTrans.clear();
    for(int i=0;i<(x.rows()/3);++i){
        printf("V: %f %f\n",x(3*i),x(3*i+1));
        vertexArrayTrans.push_back(Vector3d(x(3*i),x(3*i+1),0));
    }

  //  U.setZero();
    for(int i=0;i<vertexArray.size();++i){
        for(int j=0;j<handles.size();++j){
            double w=weightMatrix(i,j);
            /*
            U2(3*i   ,9*j )   = (vertexArrayRest[i].x()-handlesMoved[j].x()) *w;
            U2(3*i+1 ,9*j+1 ) = (vertexArrayRest[i].x()-handlesMoved[j].x()) *w;
            U2(3*i+2 ,9*j+2 ) = (vertexArrayRest[i].x()-handlesMoved[j].x()) *w;
            
            U2(3*i   ,9*j+3 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()) *w;
            U2(3*i+1 ,9*j+4 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()) *w;
            U2(3*i+2 ,9*j+5 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()) *w;
            */
            U2(3*i   ,9*j )   = (vertexArrayTrans[i].x()-handlesMoved[j].x()) *w;
            U2(3*i+1 ,9*j+1 ) = (vertexArrayTrans[i].x()-handlesMoved[j].x()) *w;
            U2(3*i+2 ,9*j+2 ) = (vertexArrayTrans[i].x()-handlesMoved[j].x()) *w;
            
            U2(3*i   ,9*j+3 ) = (vertexArrayTrans[i].y()-handlesMoved[j].y()) *w;
            U2(3*i+1 ,9*j+4 ) = (vertexArrayTrans[i].y()-handlesMoved[j].y()) *w;
            U2(3*i+2 ,9*j+5 ) = (vertexArrayTrans[i].y()-handlesMoved[j].y()) *w;
            
            U2(3*i   ,9*j+6)  = 1.0 *w;
            U2(3*i+1 ,9*j+7)  = 1.0 *w;
            U2(3*i+2 ,9*j+8)  = 1.0 *w;
        }
    }
    
    
    
    
    
    for(int i=0;i<handles.size();++i){
        double r = handlesRotation[i];
   //     r=0;
        
        t(9*i)   = cos(r);
        t(9*i+1) = sin(r);
        t(9*i+2) = 0;
        t(9*i+3) = -sin(r);
        t(9*i+4) = cos(r);
        t(9*i+5) = 0;
   //     t(9*i+6) = handlesMoved[i].x()+handleTransforms[i](6);
   //     t(9*i+7) = handlesMoved[i].y()+handleTransforms[i](7);
        t(9*i+6) = handlesMoved[i].x();
        t(9*i+7) = handlesMoved[i].y();
        t(9*i+8) = 1; //homogeneous coord
        
    /*    if(i==h){
            t(9*i+6) = handlesMoved[i].x()+handleTransforms[i](6);
            t(9*i+7) = handlesMoved[i].y()+handleTransforms[i](7);
        }*/
    }

    
    x = U2*t;
    vertexArray.clear();
    for(int i=0;i<(x.rows()/3);++i){
        printf("V: %f %f\n",x(3*i),x(3*i+1));
        vertexArray.push_back(Vector3d(x(3*i),x(3*i+1),0));
    }

    
}

void Mesh2D::updateHandleTransform(int h,double delta_rot){
    
    for(int i=0;i<handles.size();++i){
        handleTransforms[i](6) = handlesMoved[i].x()-handles[i].x();
        handleTransforms[i](7) = handlesMoved[i].y()-handles[i].y();
      //  double r = handlesRotation[i];
        //  double r;
        //  if(h==i)
        //      r=delta_rot;
        //  else
        //      r=0;
        t(9*i)   = 1;
        t(9*i+1) = 0;
        t(9*i+2) = 0;
        t(9*i+3) = 0;
        t(9*i+4) = 1;
        t(9*i+5) = 0;
        t(9*i+6) = handleTransforms[i](6);
        t(9*i+7) = handleTransforms[i](7);
        //      t(9*i+6) = handlesMoved[i].x();
        //      t(9*i+7) = handlesMoved[i].y();
        //       t(9*i+6) = handleTransforms[i](6)-handlesMoved[i].x();
        //       t(9*i+7) = handleTransforms[i](7)-handlesMoved[i].y();
        t(9*i+8) = 1; //homogeneous coord
    }

    VectorXd x = U*t;
    
    vertexArrayTrans.clear();
    for(int i=0;i<(x.rows()/3);++i){
        printf("V: %f %f\n",x(3*i),x(3*i+1));
        vertexArrayTrans.push_back(Vector3d(x(3*i),x(3*i+1),0));
    }

    
    for(int i=0;i<vertexArray.size();++i){
        for(int j=0;j<handles.size();++j){
            double w=weightMatrix(i,j);
            Vector3d trans=handlesMoved[j]-handles[j];
         /*
            U2(3*i   ,9*j )   = (vertexArrayRest[i].x()-handlesMoved[j].x()-trans.x()) *w;
            U2(3*i+1 ,9*j+1 ) = (vertexArrayRest[i].x()-handlesMoved[j].x()-trans.x()) *w;
            U2(3*i+2 ,9*j+2 ) = (vertexArrayRest[i].x()-handlesMoved[j].x()-trans.x()) *w;
            
            U2(3*i   ,9*j+3 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()-trans.y()) *w;
            U2(3*i+1 ,9*j+4 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()-trans.y()) *w;
            U2(3*i+2 ,9*j+5 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()-trans.y()) *w;
            */
            
            U2(3*i   ,9*j )   = (vertexArrayTrans[i].x()-handlesMoved[j].x()) *w;
            U2(3*i+1 ,9*j+1 ) = (vertexArrayTrans[i].x()-handlesMoved[j].x()) *w;
            U2(3*i+2 ,9*j+2 ) = (vertexArrayTrans[i].x()-handlesMoved[j].x()) *w;
            
            U2(3*i   ,9*j+3 ) = (vertexArrayTrans[i].y()-handlesMoved[j].y()) *w;
            U2(3*i+1 ,9*j+4 ) = (vertexArrayTrans[i].y()-handlesMoved[j].y()) *w;
            U2(3*i+2 ,9*j+5 ) = (vertexArrayTrans[i].y()-handlesMoved[j].y()) *w;
            
            /*
            U2(3*i   ,9*j )   = (vertexArrayRest[i].x()-handlesMoved[j].x()) *w;
            U2(3*i+1 ,9*j+1 ) = (vertexArrayRest[i].x()-handlesMoved[j].x()) *w;
            U2(3*i+2 ,9*j+2 ) = (vertexArrayRest[i].x()-handlesMoved[j].x()) *w;
            
            U2(3*i   ,9*j+3 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()) *w;
            U2(3*i+1 ,9*j+4 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()) *w;
            U2(3*i+2 ,9*j+5 ) = (vertexArrayRest[i].y()-handlesMoved[j].y()) *w;
            */
            U2(3*i   ,9*j+6)  = 1.0 *w;
            U2(3*i+1 ,9*j+7)  = 1.0 *w;
            U2(3*i+2 ,9*j+8)  = 1.0 *w;
        }
    }
    
    for(int i=0;i<handles.size();++i){
        handleTransforms[i](6) = handlesMoved[i].x()-handles[i].x();
        handleTransforms[i](7) = handlesMoved[i].y()-handles[i].y();
        double r = handlesRotation[i];
      //  double r;
      //  if(h==i)
      //      r=delta_rot;
      //  else
      //      r=0;
        t(9*i)   = cos(r);
        t(9*i+1) = sin(r);
        t(9*i+2) = 0;
        t(9*i+3) = -sin(r);
        t(9*i+4) = cos(r);
        t(9*i+5) = 0;
    //    t(9*i+6) = handlesMoved[i].x()+handleTransforms[i](6);
    //    t(9*i+7) = handlesMoved[i].y()+handleTransforms[i](7);
        t(9*i+6) = handlesMoved[i].x();
        t(9*i+7) = handlesMoved[i].y();
 //       t(9*i+6) = handleTransforms[i](6)-handlesMoved[i].x();
 //       t(9*i+7) = handleTransforms[i](7)-handlesMoved[i].y();
        t(9*i+8) = 1; //homogeneous coord
    }
    
    x = U2*t;

    vertexArray.clear();
    for(int i=0;i<(x.rows()/3);++i){
        printf("V: %f %f\n",x(3*i),x(3*i+1));
        vertexArray.push_back(Vector3d(x(3*i),x(3*i+1),0));
    }
    
    
}

