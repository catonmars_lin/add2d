//
//  mesh2D.hpp
//  ADD2D
//
//  Created by User on 2016/10/23.
//  Copyright © 2016年 Stony Brook University. All rights reserved.
//

#ifndef Mesh2D_hpp
#define Mesh2D_hpp

#include "common.h"
#include "texture.hpp"
#include "util.hpp"
#include "mosek.h"

class Handle{
public:
    Eigen::Vector3d position;
    Eigen::Matrix4d transform;
    
    Handle(){position=Eigen::Vector3d(0,0,0);
        transform=Eigen::Matrix4d::Identity();};
    ~Handle(){};
    
};

enum CONSTRAIN_TYPE{
    SPRING,
    ATTACH,
    KEYFRAME_HANDLE,
    EXAMPLE
};

class Constraint{
public:
    Eigen::Vector2d p1;
    Eigen::Vector2d p2;
    Eigen::Vector2d p3;
    Eigen::SparseMatrix<double> S;
    double rest_length;
    double w; //stiffness or weight
    
    CONSTRAIN_TYPE type;
    int idx_p1;
    int idx_p2;
    int idx_p3;
    
    Constraint(Vector2d p1,Vector2d exp1, Vector2d exp2,int idx,double w){
        this->idx_p1=idx;
        this->p1=p1;
        this->p2=exp1;
        this->p3=exp2;
        this->type=EXAMPLE;
        this->w=w;
        rest_length=0;
        
    };
    
    Constraint(Vector2d p1,Vector2d p2,int idx1,int idx2,CONSTRAIN_TYPE t,double w){
        this->p1 = p1;
        this->p2 = p2;
        this->idx_p1=idx1;
        this->idx_p2=idx2;
        this->type = t;
        this->w=w;
        switch(type){
            case ATTACH:
            case SPRING:
                rest_length = (p1 - p2).norm();
                break;
            case EXAMPLE:
            case KEYFRAME_HANDLE:
                rest_length=0;
                break;
            default:
                break;
        }
    };
    ~Constraint(){};
};


class Example{
public:
    std::vector<Eigen::Vector3d> r;
    std::vector<Eigen::Vector3d> t;
    double time;
    
    
public:
    Example(std::vector<Eigen::Vector3d> handles){
        r.reserve(handles.size());
        copy(handles.begin(),handles.end(),back_inserter(r));
        t.reserve(handles.size());
        copy(handles.begin(),handles.end(),back_inserter(t));
    };
    ~Example(){};
    void convToDiffCoord(){
        Eigen::Vector3d centerOfMass;
        centerOfMass.setZero();
        for(auto i:r){
            centerOfMass+=i;
        }
        centerOfMass/=((double)r.size());
        for(int i=0;i<t.size();++i){
            t[i]=r[i]-centerOfMass;
        }
    };

};

class Mesh2D{
    
private:
    std::vector<std::vector<int>> vertexNeighbors;
    std::vector<Eigen::Vector3d> vertexArrayRest;
    std::vector<Eigen::Vector3d> vertexArrayTrans;
    std::vector<Eigen::Vector3d> vertexArray;
    std::vector<Eigen::Vector3i> triIndexArray;
    
    std::vector<Eigen::Vector2i> edgeIndexArray;
    std::vector<Eigen::Vector3d> edgeArray;
    
    std::vector<Eigen::Vector2d> uvArray;
    
    
    
    Eigen::Matrix4d transform;
    
    std::vector<Eigen::Vector3d> handles;
    std::vector<bool> IsHandleKeyframed;
    std::vector<Eigen::Vector2i> handleEdges;
    
    std::vector<Constraint*> Cs;
    std::vector<Constraint*> CKeys;
    
    Eigen::MatrixXd weightMatrix;
    
    Eigen::MatrixXd M;
    Eigen::SparseMatrix<double> U;
    Eigen::MatrixXd U2;
    Eigen::VectorXd t;
    Eigen::MatrixXd Mp;
    Eigen::MatrixXd Mp_inv;
    Eigen::VectorXd f_ext_p;
    
    Eigen::VectorXd sn;
    Eigen::VectorXd qn;
    Eigen::VectorXd tn;
    Eigen::VectorXd tn1;
    Eigen::VectorXd vn;
    Eigen::VectorXd f_ext;
    Eigen::VectorXd p; //Projected position
    Eigen::VectorXd rhs;
    Eigen::LLT<Eigen::MatrixXd> lhs;
    
    
    

    
    
    double h;
    
    GLuint VertexArrayID;
    GLuint vertexbuffer;
    
    GLuint texVertexArrayID;
    GLuint texVertexbuffer;
    
    GLuint uvArrayID;
    GLuint uvBuffer;

    
    GLuint handleVertexArrayID;
    GLuint handleVertexBuffer;
    
    GLuint EdgeVertexArrayID;
    GLuint edgeVertexBuffer;
    
    GLuint EdgeIndexBufferID;
    GLuint edgeIndexBuffer;
    
   // GLuint TriVertexArrayID;
    GLuint TriIndexBufferID;
    GLuint triIndexBuffer;
    
    GLuint programID_point;
    GLuint programID_line;
    GLuint programID_tri;
    
    GLTexture* tex;
    double texScale;
public:
    bool haveTex;
    bool isPhysics;
    bool drawWires;
    std::vector<Eigen::Vector3d> handlesMoved;
    std::vector<Eigen::Matrix<double,9,1>> handleTransforms;
    std::vector<double> handlesRotation;
    std::vector<Eigen::Vector3d> ex_t;
    std::vector<Example> exManifold;

    
    Mesh2D(){
        glGenVertexArrays(1, &VertexArrayID);
        glGenBuffers(1, &vertexbuffer);
        glGenVertexArrays(1, &handleVertexArrayID);
        glGenBuffers(1, &handleVertexBuffer);
        glGenVertexArrays(1, &EdgeVertexArrayID);
        glGenBuffers(1, &edgeIndexBuffer);
        glGenVertexArrays(1, &TriIndexBufferID);
        glGenBuffers(1, &triIndexBuffer);
        
        glGenVertexArrays(1, &texVertexArrayID);
        glGenBuffers(1, &texVertexbuffer);
        
        glGenVertexArrays(1, &uvArrayID);
        glGenBuffers(1, &uvBuffer);

        // Create and compile our GLSL program from the shaders
        programID_point = LoadShaders( "vs_point.vs", "fs_point.fs" );
        programID_line = LoadShaders( "vs_line.vs", "fs_line.fs" );
        programID_tri = LoadShaders( "vs_tri.vs", "fs_tri.fs" );

        h=0.02; //simulate 50 fps
        texScale=0.75;
        isPhysics=true;
        haveTex=false;

    };
    ~Mesh2D();
    
    int load(std::string pathname);
    int save(const char* pathname);
    int loadTexture(std::string pathname);
    int addVertex(Eigen::Vector3d v);
    int deleteVertex(Eigen::Vector3d* v);
    int addHandle(Eigen::Vector3d h);
    void assignWeight();
    int mosekOptimize(int wi,
                      int hIdx,
                      std::vector<int> handleIdx,
                      Eigen::SparseMatrix<double> LMLInv);
    int computeBoundedBiharmonicWeight();
    void triangularize();
    void triangularizeHandles();
    void setupMatrices();
    void resetPhysics();
    void update();
    void updateOneFrame();
    void simulate();
    void draw();
    void drawRefTex();
    int nearestHandle(Eigen::Vector2d v);
    void setHandleTransform(int i,Eigen::Vector2d nv);
    void updateHandleTransform(int i, double delta_rot);
    void setKeyframeHandle(int i,Eigen::Vector2d nv);
    
    void addCurrentPoseToExample(double time);
    
    std::vector<Eigen::Vector3d> getHandles(){
        return handlesMoved;
    }
    
    
};
#endif /* mesh2D_hpp */
