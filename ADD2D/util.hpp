//
//  util.hpp
//  ADD2D
//
//  Created by User on 2016/10/30.
//  Copyright © 2016年 Stony Brook University. All rights reserved.
//

#ifndef util_hpp
#define util_hpp

#include "common.h"
#include <algorithm>
#include <cmath>
using namespace Eigen;

Vector2d cursorToViewport(Vector2d v,int viewport_width,int viewport_height);
GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path);
double angle2D(Vector2d x,Vector2d y);
bool contains(std::vector<int> vec,int val);
std::vector<int> intersec(std::vector<int> v1,std::vector<int> v2);

#endif /* util_hpp */
