#version 410 core

#define resolution vec2(1280.0, 720.0)
#define Thickness 15.0

// Ouput data
out vec4 color;

float drawLine(vec2 p1, vec2 p2) {
    vec2 uv = gl_FragCoord.xy / resolution.xy;
    
    float a = abs(distance(p1, uv));
    float b = abs(distance(p2, uv));
    float c = abs(distance(p1, p2));
    
    if ( a >= c || b >=  c ) return 0.0;
    
    float p = (a + b + c) * 0.5;
    
    // median to (p1, p2) vector
    float h = 2 / c * sqrt( p * ( p - a) * ( p - b) * ( p - c));
    
    return mix(1.0, 0.0, smoothstep(0.5 * Thickness, 1.5 * Thickness, h));
}

void main()
{
    color = vec4(0.8,0.2,0.8,1);
}