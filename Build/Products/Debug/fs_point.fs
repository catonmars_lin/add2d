#version 410 core

// Ouput data
out vec4 color;
uniform vec4 pointColor;

void main()
{
    vec2 circCoord = 2.0 * gl_PointCoord - 1.0;
    if (dot(circCoord, circCoord) > 1.0) {
        discard;
    }
	// Output color = red 
	color = pointColor;

}