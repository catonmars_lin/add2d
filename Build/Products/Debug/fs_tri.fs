#version 410 core

#define resolution vec2(1280.0, 720.0)

// Interpolated values from the vertex shaders
in vec2 UV;

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D myTextureSampler;

void main()
{
    color = vec4(0.8,0.0,0.8,1);
    color = texture( myTextureSampler, UV ).rgba;
}